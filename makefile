splunk_inst = /home/wubin/tools/splunk
splunk_auth = -auth admin:garlic123
splunkf_inst = /home/wubin/tools/splunkforwarder
splunkf_auth = -auth admin:changeme
root_dir = /home/wubin/workspace/edanz/steward
log_dir = $(root_dir)/log
log_file = steward
log_ext = log
pid_file = $(log_dir)/steward.pid
debug_strings = loopback:connector:soap

#Add by Yexing on 20150325 for easier docker related commands:
USER=$(shell whoami)
CONTAINERNAME=systembackbone

.PHONY:
run:
	@rm -rf $(log_dir)/*
	@slc run --cluster cpus -d --pid $(pid_file) -l $(log_dir)/$(log_file).%w.%p.$(log_ext) --no-profile

.PHONY:
debug_log:
	@rm -rf $(log_dir)/*
	@DEBUG=$(debug_strings) slc run --cluster cpus -d -l $(log_dir)/$(log_file).$(log_ext) --no-profile

.PHONY:
debug:
	@rm -rf $(log_dir)/*
	@slc debug server/server.js

.PHONY:
status:
	@-slc runctl status

.PHONY:
restart:
	@slc runctl restart

.PHONY:
stop:
	@slc runctl stop

.PHONY:
up:
	@git pull

.PHONY:
ci:
	@git add . && git commit && git push

.PHONY:
sp_start:
	@sudo $(splunk_inst)/bin/splunk start
	@sudo $(splunk_inst)/bin/splunk enable listen 9997 $(splunk_auth)

.PHONY:
sp_stop:
	@sudo $(splunk_inst)/bin/splunk disable listen 9997 $(splunk_auth)
	@sudo $(splunk_inst)/bin/splunk stop

.PHONY:
spf_start:
	@sudo $(splunkf_inst)/bin/splunk add forward-server 54.64.197.126:9997 $(splunkf_auth)
	@sudo $(splunkf_inst)/bin/splunk start
	@sudo $(splunkf_inst)/bin/splunk add monitor /var/log/syslog $(splunkf_auth)

.PHONY:
spf_stop:
	@sudo $(splunkf_inst)/bin/splunk remove monitor /var/log/syslog $(splunkf_auth)
	@sudo $(splunkf_inst)/bin/splunk remove forward-server 54.64.197.126:9997 $(splunkf_auth)
	@sudo $(splunkf_inst)/bin/splunk stop

build:
	docker build -t $(USER)/$(CONTAINERNAME) .
rebuild:
	docker build --no-cache -t $(USER)/$(CONTAINERNAME) .
destroy:
	docker stop $(CONTAINERNAME) 
	docker rm $(CONTAINERNAME) 
#run: 
	#docker run -d -p 3000:3000 --name $(CONTAINERNAME) $(USER)/$(CONTAINERNAME) 
#debug: 
	#docker exec -it $(CONTAINERNAME) /bin/bash
#start: 
	#docker start $(CONTAINERNAME) 
#stop: 
	#docker stop $(CONTAINERNAME) 
#logs: 
	#docker logs -f $(CONTAINERNAME) 
