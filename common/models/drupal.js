var
sugarUserName = 'webformuser',
sugarUserPassword = 'edanz1002',
module_name = 'Contacts',
order_by = 'date_entered',
offset = '0',
max_results = '1',
deleted = '0';

module.exports = function(Drupal) {

  Drupal.getUser = function(email, select_fields, cb) {

    var server = require('../../server/server.js');
    var SoapData = server.models.SoapData;

    if(select_fields != null) {
      select_fields = select_fields.split(",");
    }else{
      select_fields = ["id", "first_name", "last_name", "email1"];
    }

      SoapData.sugarlogin(sugarUserName, sugarUserPassword, function(err, result) {
          if(!err) {

            var session = result,
            query = '(contacts.email1 = "' + email + '")';

            SoapData.getEntryList(
              session,
              module_name,
              query,
              order_by,
              offset,
              select_fields,
              max_results,
              deleted,
            function(err, result) {
              if(!err) {
                cb(err, result);
              }
            });
          
          }

      });
  
  }
  Drupal.remoteMethod(
    'getUser',
    {
      accepts: [
        {arg: 'email', type: 'string', required:true},
        {arg: 'select_fields', type: 'string', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/get_user'}
    });


  Drupal.updateUser = function(
    ws_user, cb
  ) {

    var server = require('../../server/server.js');
    var SoapData = server.models.SoapData;
    ws_user = JSON.parse(ws_user);
    SoapData.sugarlogin(sugarUserName, sugarUserPassword, function(err, result) {
        if (!err) {

            var session = result,
            query = '(contacts.email1 = "' + ws_user.email1 + '")',
            select_fields = ["id"];

            SoapData.getEntryList(
              session,
              module_name,
              query,
              order_by,
              offset,
              select_fields,
              max_results,
              deleted,
            function(err, result) {
                if(!err) {
                    var entry_id = result.item.id.$value;
                    if(ws_user.form_id == 'user_profile_form'){
                      name_value_list = [
                      {"name": "id", "value": entry_id},
                      {"name": "first_name", "value": ws_user.first_name},
                      {"name": "last_name", "value": ws_user.last_name},
                      {"name": "email1", "value": ws_user.email1},
                      {"name": "country_c", "value": ws_user.country_c},
                      {"name": "phone_mobile", "value": ws_user.phone_mobile}
                      ];
                    }else if(ws_user.form_id == 'profile2_edit_main_form') {
                      name_value_list = [
                      {"name": "id", "value": entry_id},
                      {"name": "email2", "value": ws_user.email2},
                      {"name": "phone_work", "value": ws_user.phone_work},
                      {"name": "phone_fax", "value": ws_user.phone_fax},
                      {"name": "institution_c", "value": ws_user.institution_c},
                      {"name": "department", "value": ws_user.department},
                      {"name": "primary_address_postalcode", "value": ws_user.primary_address_postalcode}
                      ];
                    }
                    SoapData.setEntry(
                      session,
                      module_name,
                      name_value_list,
                      function(err, result){
                        if(!err){
                          cb(err, result);
                        }
                      });
                }
            });
        }
    });
  }

  Drupal.remoteMethod(
    'updateUser',
    {
      accepts: [
        {arg: 'ws_user', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/update_user'}
    });

};
