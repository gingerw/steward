module.exports = function(Service) {
  Service.greet = function(msg, cb) {
    cb(null, 'Greetings ... ' + msg);
  }

  Service.remoteMethod(
    'greet',
    {
      accepts: {arg: 'msg', type: 'string'},
      returns: {arg: 'greeting', type: 'string'},
      http: {verb: 'post', path: '/greet'}
    }
  );

  Service.createPerson = function(
    email, first_name, last_name, cb
  ) {
    var server = require('../../server/server.js');
    var Person = server.models.Person;

    Person.exists(email, function(err, exists) {
      if (exists) {
        cb('Email already exists.', null);
      } else {
        Person.create(
        {
          email: email,
          first_name: first_name,
          last_name: last_name,
          date_created: new Date(),
          date_modified: new Date()
        },
        function(err, result) {
          if(!err) {
            cb(null, result);
          } else {
            cb('Failed to create person with email' + email, null);
          }
        });
      }
    });
  }

  Service.remoteMethod(
    'createPerson',
    {
      accepts: [
        {arg: 'email', type: 'string'},
        {arg: 'first_name', type: 'string'},
        {arg: 'last_name', type: 'string'}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/createPerson'}
    });
};
