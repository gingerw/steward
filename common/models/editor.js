module.exports = function(Editor) {
  Editor.greet = function(msg, cb) {
    cb(null, 'Greetings ... ' + msg);
  }

  Editor.remoteMethod(
    'greet',
    {
      accepts: {arg: 'msg', type: 'string'},
      returns: {arg: 'greeting', type: 'string'}
    }
  );
};
