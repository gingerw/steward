module.exports = function(SoapData) {
  /* sugar soap api login function */
  SoapData.sugarlogin = function(
    username,
    password,
    cb
  ) {
    console.time('sugarlogin');

    SoapData.login(
      {
        user_auth: {
          user_name: username,
          password: password
        },
        application_name: 'sysbackbone'
      }, function (err, response) {
        console.trace('Login response: %j', response);

        cb(err, response.return.id.$value);
      }
    );

    console.timeEnd('sugarlogin');
  }

  SoapData.remoteMethod(
    'sugarlogin',
    {
      accepts: [
        {arg: 'username', type: 'string', required: true},
        {arg: 'password', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/login'}
    }
  );
  SoapData.remoteMethod(
    'sugarlogin',
    {
      accepts: [
        {arg: 'username', type: 'string', required: true},
        {arg: 'password', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/login'}
    }
  );

  /* sugar soap api get_user_id */
  SoapData.getUserId = function(
    session,
    cb
  ) {
    console.time('getUserId');

    SoapData.get_user_id(
      {
        session: session
      }, function (err, response) {
        console.trace('get_user_id response: %j', response);

        cb(err, response.return.$value);
      }
    );

    console.timeEnd('getUserId');
  }

  SoapData.remoteMethod(
    'getUserId',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/get_user_id'}
    }
  );
  SoapData.remoteMethod(
    'getUserId',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/get_user_id'}
    }
  );

  /* sugar soap api get_entry_list */
  SoapData.getEntryList = function(
    session,
    module_name,
    query,
    order_by,
    offset,
    select_fields,
    max_results,
    deleted,
    cb
  ) {
    console.time('getEntryList');

    SoapData.get_entry_list(
      {
        session: session,
        module_name: module_name,
        query: query,
        order_by: order_by,
        offset: offset,
        select_fields: select_fields || [],
        max_results: max_results,
        deleted: deleted || 0
      }, function (err, response) {
        console.trace('get_entry_list response: %j', response);

        cb(err, response.return.entry_list);
      }
    );

    console.timeEnd('getEntryList');
  }

  SoapData.remoteMethod(
    'getEntryList',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'query', type: 'string', required: true},
        {arg: 'order_by', type: 'string', required: true},
        {arg: 'offset', type: 'number', required: true},
        {arg: 'select_fields', type: 'array', required: false},
        {arg: 'max_results', type: 'number', required: true},
        {arg: 'deleted', type: 'number', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/get_entry_list'}
    }
  );
  SoapData.remoteMethod(
    'getEntryList',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'query', type: 'string', required: true},
        {arg: 'order_by', type: 'string', required: true},
        {arg: 'offset', type: 'number', required: true},
        {arg: 'select_fields', type: 'array', required: false},
        {arg: 'max_results', type: 'number', required: true},
        {arg: 'deleted', type: 'number', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/get_entry_list'}
    }
  );

  /* sugar soap api get_entry */
  // select_fields: ["id", "first_name"]
  SoapData.getEntry = function(
    session,
    module_name,
    id,
    select_fields,
    cb
  ) {
    console.time('getEntry');

    SoapData.get_entry(
      {
        session: session,
        module_name: module_name,
        id: id,
        select_fields: select_fields || []
      }, function (err, response) {
        console.trace('get_entry response: %j', response);

        cb(err, response.return.entry_list);
      }
    );

    console.timeEnd('getEntry');
  }

  SoapData.remoteMethod(
    'getEntry',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'id', type: 'string', required: true},
        {arg: 'select_fields', type: 'array', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/get_entry'}
    }
  );
  SoapData.remoteMethod(
    'getEntry',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'id', type: 'string', required: true},
        {arg: 'select_fields', type: 'array', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/get_entry'}
    }
  );

  /* sugar soap api get_entries */
  SoapData.getEntries = function(
    session,
    module_name,
    ids,
    select_fields,
    cb
  ) {
    console.time('getEntries');

    SoapData.get_entries(
      {
        session: session,
        module_name: module_name,
        ids: ids || [],
        select_fields: select_fields || []
      }, function (err, response) {
        console.trace('get_entries response: %j', response);

        cb(err, response.return.entry_list);
      }
    );

    console.timeEnd('getEntries');
  }

  SoapData.remoteMethod(
    'getEntries',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'ids', type: 'array', required: false},
        {arg: 'select_fields', type: 'array', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/get_entries'}
    }
  );
  SoapData.remoteMethod(
    'getEntries',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'ids', type: 'array', required: true},
        {arg: 'select_fields', type: 'array', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/get_entries'}
    }
  );

  /* sugar soap api set_entry */
  // name_value_list:
  // [{"name": "id", "value": "39c04a58-c783-5899-499d-54363bf2a9cd"}, {"name": "first_name", "value": "Bin"}]
  SoapData.setEntry = function(
    session,
    module_name,
    name_value_list,
    cb
  ) {
    console.time('setEntry');

    SoapData.set_entry(
      {
        session: session,
        module_name: module_name,
        name_value_list: name_value_list || []
      }, function (err, response) {
        console.trace('set_entry response: %j', response);

        cb(err, response.return.entry_list);
      }
    );

    console.timeEnd('setEntry');
  }

  SoapData.remoteMethod(
    'setEntry',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'name_value_list', type: 'array', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/set_entry'}
    }
  );
  SoapData.remoteMethod(
    'setEntry',
    {
      accepts: [
      {arg: 'session', type: 'string', required: true},
      {arg: 'module_name', type: 'string', required: true},
      {arg: 'name_value_list', type: 'array', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/set_entry'}
    }
  );

  /* sugar soap api get_relationships */
  SoapData.getRelationships = function(
    session,
    module_name,
    module_id,
    related_module,
    related_module_query,
    deleted,
    cb
  ) {
    console.time('getRelationships');

    SoapData.get_relationships(
      {
        session: session,
        module_name: module_name,
        module_id: module_id,
        related_module: related_module,
        related_module_query: related_module_query,
        deleted: deleted || 0
      }, function (err, response) {
        console.trace('get_relationships response: %j', response);

        cb(err, response.return.ids);
      }
    );

    console.timeEnd('getRelationships');
  }

  SoapData.remoteMethod(
    'getRelationships',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'module_id', type: 'string', required: true},
        {arg: 'related_module', type: 'string', required: true},
        {arg: 'related_module_query', type: 'string', required: false},
        {arg: 'deleted', type: 'number', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/get_relationships'}
    }
  );
  SoapData.remoteMethod(
    'getRelationships',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module_name', type: 'string', required: true},
        {arg: 'module_id', type: 'string', required: true},
        {arg: 'related_module', type: 'string', required: true},
        {arg: 'related_module_query', type: 'string', required: false},
        {arg: 'deleted', type: 'number', required: false}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/get_relationships'}
    }
  );
  
  /* sugar soap api set_relationship */
  SoapData.setRelationship = function(
    session,
    module1,
    module1_id,
    module2,
    module2_id,
    cb
  ) {
    console.time('setRelationship');

    SoapData.set_relationship(
      {
        session: session,
        set_relationship_value: {
          module1: module1,
          module1_id: module1_id,
          module2: module2,
          module2_id: module2_id
        }
      }, function (err, response) {
        console.trace('set_relationship response: %j', response);

        cb(err, response);
      }
    );

    console.timeEnd('setRelationship');
  }

  SoapData.remoteMethod(
    'setRelationship',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module1', type: 'string', required: true},
        {arg: 'module1_id', type: 'string', required: true},
        {arg: 'module2', type: 'string', required: true},
        {arg: 'module2_id', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/set_relationship'}
    }
  );
  SoapData.remoteMethod(
    'setRelationship',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'module1', type: 'string', required: true},
        {arg: 'module1_id', type: 'string', required: true},
        {arg: 'module2', type: 'string', required: true},
        {arg: 'module2_id', type: 'string', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/set_relationship'}
    }
  );
  
  /* sugar soap api set_relationships */
  // set_relationship_list:
  // [{"module1": "A", "module1_id": "39c04a58-c783-5899-499d-54363bf2a9cd", "module2": "B", "module2_id": "39c04a58-c783-5899-499d-54363bf2a9cf"} ... ]
  SoapData.setRelationships = function(
    session,
    set_relationship_list,
    cb
  ) {
    console.time('setRelationships');

    SoapData.set_relationship(
      {
        session: session,
        set_relationship_list: set_relationship_list || []
      }, function (err, response) {
        console.trace('set_relationships response: %j', response);

        cb(err, response);
      }
    );

    console.timeEnd('setRelationships');
  }

  SoapData.remoteMethod(
    'setRelationships',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'set_relationship_list', type: 'array', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'post', path: '/set_relationships'}
    }
  );
  SoapData.remoteMethod(
    'setRelationships',
    {
      accepts: [
        {arg: 'session', type: 'string', required: true},
        {arg: 'set_relationship_list', type: 'array', required: true}
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {verb: 'get', path: '/set_relationships'}
    }
  );
};
