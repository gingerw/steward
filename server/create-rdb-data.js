var server = require('./server.js');

var Person = server.models.Person;
//var Person = app.models.Person; // in server.js

Person.count(null, function(err, c) {
  if (!err && c === 0) {
    console.log('No person found, creating some sample data.');

    var persons = [
    {
      email: "foo@bar.com",
      first_name: "f",
      last_name: "oo",
      date_created: new Date(),
      date_modified: new Date()
    }, {
      email: "bar@bar.com",
      first_name: "b",
      last_name: "ar",
      date_created: new Date(),
      date_modified: new Date()
    }
    ];

    var count = persons.length;
    persons.forEach(function (p){
      Person.create(p, function (err, result){
        if (!err) {
          console.log('Record created:', result);
          --count;
          if (count === 0) {
            console.log('done');
          }
        }
      });
    });
  } else {
    console.log('Found some persons in DB.');
  }
});
